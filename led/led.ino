#define LED 12

void setup() 
{
  pinMode(LED,OUTPUT);
  Serial.begin(9600);
}

void loop()
{
  //LED blinking
  Led(HIGH);
  delay(1000);
  Led(LOW);
  delay(1000);  
}

void Led(int state)
{
  digitalWrite(LED,state);
}
