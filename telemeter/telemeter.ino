// Libraries
#include "TM1637.h" // 7-segment display

// Pins definitions
// TM1637
#define CLK 7
#define DIO 8
//
// Ultrasound sensor
#define TRIG_PIN 9
#define ECHO_PIN 10

// Global variables
//
// Number of digits to display 
#define NB_DIGITS 4
#define DEFAULT_VALUE 8888      // value to test all leds
// 
// Ultrasound sensor
int distance;
//
// 7-segment display
int8_t ListDisp[NB_DIGITS]; // digits to be displayed
int8_t idx = NB_DIGITS - 1;  // start with least significant digit

// 7-segment display instance
TM1637 tm1637(CLK,DIO);  

//////////// SETUP //////////////////
void setup() {
  tm1637.init();
  tm1637.set(BRIGHT_TYPICAL);//BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIGHTEST = 7;
  pinMode(TRIG_PIN, OUTPUT); // Sets the trigPin as an Output
  pinMode(ECHO_PIN, INPUT); // Sets the echoPin as an Input
  Serial.begin(9600); // Starts the serial communication
  
  display(DEFAULT_VALUE); // Display test value, 8888
  delay(1000); // wait 1 second
}

/////////// LOOP ////////////////////
void loop() {
  // Get distance from sensor
  distance = getDistanceFromUltraSoundSensor();

  // Display distance
  display(distance);
  
  // fix displayed value for 300 ms
  delay(300);
}

// Function measures distance using ultrasound sensor and returns it
int getDistanceFromUltraSoundSensor(void){
  int distance;
  long duration;
  
  // Clears the trigPin
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(ECHO_PIN, HIGH);
  
  // Find measured distance in centimeters
  distance = duration * 0.034 / 2;

  return distance;
}

// Procedure displays integer value on 4 digits 7-segment display
void display(int val){
  // initialize all digits to OFF state 
  for(int i = 0; i<sizeof(ListDisp); i++){
     ListDisp[i] = 0x7f;
  }

  // Get digit by digit from integer
  //   starting with least significant digit (idx = 3)
  while(val > 0){
    // Get the least significant digit
    ListDisp[idx] = val % 10;
    
    // Update value for next digit treatment
    val = val / 10;

    if(val == 0 or idx <= 0){ // Done processing integer value 
      idx = NB_DIGITS - 1;
    } else {
      idx--; // next digit
    }
  }
  
  // Display digits
  tm1637.display(ListDisp);
}
