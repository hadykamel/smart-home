int SENSOR = 4;              // the pin that the sensor is attached to
int SensorState = LOW;       // by default, no motion detected
int SensorVal = 0;           // variable to store the sensor status (value)


void setup() 
{
  pinMode(SENSOR, INPUT);    // initialize sensor as an input
  Serial.begin(9600);        // initialize serial
}


void loop() 
{
  if (motionDetected() == 1) 
  {
    Serial.print("Detection");
  }
}


int motionDetected(void) 
{
  int motion = 0;
  SensorVal = digitalRead(SENSOR);
  
  if (SensorVal == HIGH) 
  { //a motion is detected
    if (SensorState == LOW) 
    { //if there was no motion at first 
      motion = 1;
      SensorState = HIGH;  //a motion was detected
    }
  } 
  else 
  { //motion has stopped 
    if (SensorState == HIGH) 
    { 
      SensorState = LOW;
    }
  }
  
  return motion;  //1 for motion detection, 0 if not
}
