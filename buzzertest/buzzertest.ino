// Buzzer connected to pin #11
#define BUZZER  11

int i = 0;

void setup()
{
  pinMode(BUZZER, OUTPUT);
}

void loop()
{
  buzzerOn();
}

//Procedure to turn on Buzzer
void buzzerOn(void)
{
  digitalWrite(BUZZER,HIGH);    //make sound
  delay(1000);                  //wait 1 second
  digitalWrite(BUZZER,LOW);     //stop sound
  delay(1000);                  //wait 1 second
}
