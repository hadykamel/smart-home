The goal of this project is to make my home connected and smart using Arduino and potentially Raspberry pi.

For the moment v1.0 consists of an arduino collecting data from sensors (ultrasound, motion, temperature, humidity)
and sending commands to actuators (buzzer, led) or to display (4-digit display).